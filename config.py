import os
import sys
from yaml import load, dump, scanner
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

try:
    config_file = open(os.getcwd()+os.sep+'config.yml')
    config = load(config_file, Loader=Loader)
except FileNotFoundError:
    print("Fatal: Config file is not found.")
    sys.exit(1)  # For @twenshin: 0 is successful termination, 1 is fatal error termination, 2 is CLI syntax error
except scanner.ScannerError:
    print("Fatal: Corrupt config file. Please check it and fix it.")
    sys.exit(2)
