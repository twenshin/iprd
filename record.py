# Built-ins
import os
import sys
import datetime
import pathlib
from multiprocessing import Process, ProcessError, current_process
# Installable via package manager (pip)
import requests
import argparse
import tornado.ioloop
from PyInquirer import style_from_dict, Token, prompt
from PyInquirer import Validator, ValidationError
#
from config import config
import server
from validators import ExternalPlaylistValidator


# url: http://188.35.9.26:14031/udp/174m for example
# endtime: '%Y-%m-%d %H:%M:%S'
# filename: name in records folder
def run(url, until, filename=None):
    if filename is None:
        filename = datetime.datetime.now().strftime('%Y%m%d%H%M%S')+"_record.mp4"
    outdir = os.path.join(os.getcwd(), 'records')
    outfile = open(os.path.join(outdir, filename), 'wb')
    with requests.Session() as session:
        stream = requests.get(url, stream=True)
        for chunk in stream.iter_content(chunk_size=1024):
            if datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') == until:
                break
            if chunk:
                outfile.write(chunk)
    print("Done!")


def record(settings):
    print("Process {} started!".format(current_process().name))
    name = settings['name']
    stream_url = settings['stream_url']
    time_begin = settings['time_begin']
    time_end = settings['time_end']
    repeat = settings['repeat']
    outdir = pathlib.Path('.', 'records')
    print(outdir)
    no_more_records = False
    while True:
        if datetime.datetime.now().strftime('%H:%M:%S') == time_begin and repeat and datetime.datetime.today().weekday() in repeat:
            filename = "{}_{}_{}-{}.mp4".format(name.replace(' ', '_'), datetime.datetime.now().strftime('%Y-%m-%d'), time_begin,
                                                time_end)
            file_path = pathlib.Path(outdir, filename)
            print(file_path)
            outfile = open(r"{}".format(file_path), 'wb')
            print("Recording of {} began in file {}".format(name, filename))
            with requests.Session() as session:
                stream = requests.get(stream_url, stream=True)
                for chunk in stream.iter_content(chunk_size=1024):
                    if datetime.datetime.now().strftime('%H:%M:%S') == time_end:
                        break
                    if chunk:
                        outfile.write(chunk)
            if repeat is None:
                no_more_records = True
        if no_more_records:
            break


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', type=str, help="Run mode - config or serve")
    args = parser.parse_args()
    if args.mode == 'serve':
        port = config['server']['port']
        processes = list()
        for rec in config['records']:
            proc = Process(target=record, name=rec['name'], args=(rec,))
            proc.start()
            processes.append(proc)
        print("Record watcher started!")
        print("Spinning up a web server on port {}.".format(port))
        srv = server.app()
        srv.listen(port)
        tornado.ioloop.IOLoop.current().start()
    elif args.mode == 'config':
        style = style_from_dict({
            Token.QuestionMark: '#E91E63 bold',
            Token.Selected: '#673AB7 bold',
            Token.Instruction: '',  # default
            Token.Answer: '#2196f3 bold',
            Token.Question: '',
        })
        playlist_type_question = {
            'name': 'playlist_type',
            'type': 'list',
            'message': 'What is your playlist location? Pick local to use m3u file in ./playlists directory, or external to use a url for your playlist',
            'choices': ['Local', 'External']
        }
        playlist_type = prompt(playlist_type_question, style=style)
        if playlist_type['playlist_type'] == "Local":
            playlists = [file for file in os.listdir('playlists')]
            if len(playlists) > 0:
                playlist_location_question_local = {
                    'name': 'playlist_location_local',
                    'type': 'list',
                    'message': 'Pick your desired playlist',
                    'choices': playlists
                }
                playlist_location = prompt(playlist_location_question_local, style=style)
            else:
                print('Fatal: No playlists in playlists folder')
                sys.exit(2)
        else:
            playlist_location_question_external = {
                'name': 'playlist_location_external',
                'type': 'input',
                'message': 'Input a URL of your desired playlist',
                'validate': ExternalPlaylistValidator
            }
            playlist_url = prompt(playlist_location_question_external, style=style)
    else:
        parser.error('Usage: python3 record.py [serve | config]')
