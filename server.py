import tornado.ioloop
import tornado.web
from config import config

def app():
    routes = [
        (r'/auth', AuthHandler)
    ]
    if config['server']['allow_web']:
        routes.extend([
            (r'/', IndexHandler)
        ])
    return tornado.web.Application(routes, debug=True)


class BaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')


class AuthHandler(BaseHandler):
    def post(self):
        pass


class IndexHandler(BaseHandler):
    def get(self):
        self.write("<h1>Index page stub</h1>")
