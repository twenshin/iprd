import requests
from PyInquirer import Validator, ValidationError


class ExternalPlaylistValidator(Validator):
    def validate(self, document):
        playlist = requests.get(document.text)
        if playlist.status_code != 200:
            raise ValidationError(
                message='No playlist found at this url.',
                cursor_position=len(document.text)
            )
        else:
            playlist_content = playlist.content
            playlist_rows = playlist_content.decode('utf-8').split('\n')
            print(playlist_rows)
            return True
